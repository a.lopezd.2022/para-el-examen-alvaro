#!/bin/sh

if [ $# -ge 1 ]
then
	if [ $# -eq 2 ]
	then
		cat $1 | sed -E 's/alice/'$2'/i'
		echo 'Numero de apariencias: ' $(cat $1 | egrep -i -c alice)
	else
		echo Inserte nombre de sustitución
		exit 22
	fi
else 
	echo Falta fichero
	exit 7
fi
